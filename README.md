BlackJack
=================================

This is a complete BlackJack Game written in Python.  It uses the command line for input and output.  It complies with the following requirements:

* The game needs to have one player versus an automated dealer.
* The player can stand or hit.
* The player must be able to pick their betting amount.
* You need to keep track of the players total money.
* You need to alert the player of wins, losses, or busts, etc...