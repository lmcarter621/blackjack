from player import Player
from deck import Deck

class BlackJack(object):
	"""Basic BlackJack"""
	def __init__(self, deck, player):
		super(BlackJack, self).__init__()
		self.deck = deck
		self.player = player
		self.dealer = Player()

	def play(self):
		self.deck.shuffle()

		self.player.hand = [self.deck.deck.pop(), self.deck.deck.pop()]
		self.dealer.hand = [self.deck.deck.pop(), self.deck.deck.pop()]

		round_counter = 1
		self.player.compute_hand_value()
		self.dealer.compute_hand_value()

		while self.player.hand_value < 21 and self.dealer.hand_value < 21:
			print 'Dealer cards: {0} Dealer value: {1}'.format(self.dealer.hand, self.dealer.hand_value)
			print 'Your cards: {0} Your value: {1}'.format(self.player.hand, self.player.hand_value)
			round_counter += 1
			# dealer continues to hit until value is 17 or greater
			if self.dealer.hand_value < 17:
				self.dealer.hit(self.deck.deck.pop())

			while True:
				value = raw_input('Enter \'h\' for hit and \'s\' for stand:' )
				if value not in ['h', 's']:
					print 'Value is not accepted.  Please try again.'
					continue
				else:
					break

			if value == 'h':
				self.player.hit(self.deck.deck.pop())
			elif value == 's' and self.player.hand_value < 21 and self.dealer.hand_value >= 17:
				break

		self.determine_winner()


	def determine_winner(self):
		print 'Dealer cards: {0} Dealer value: {1}'.format(self.dealer.hand, self.dealer.hand_value)
		print 'Your cards: {0} Your value: {1}'.format(self.player.hand, self.player.hand_value)
		if self.player.hand_value > 21:
			print 'PLAYER BUSTS!'
			self.player.bet_total -= self.player.bets.pop()
		elif self.dealer.hand_value > 21:
			print 'DEALER BUSTS!'
		elif self.player.hand_value <= 21 and self.player.hand_value > self.dealer.hand_value:
			print 'PLAYER WINS!'
		elif self.dealer.compute_hand_value <= 21 and self.dealer.hand_value > self.player.hand_value:
			print 'DEALER WINS!'
			self.player.bet_total -= self.player.bets.pop()
		elif self.player.hand_value == self.dealer.hand_value:
			print 'PUSH.  No one wins.'


if __name__ == '__main__':
	player = Player()
	game = BlackJack(Deck(), player)
	while True:
		cont = raw_input('Play Black Jack? [y/n] ')
		if cont not in ['y', 'n']:
			print 'Value is not accepted.  Please try again'
			continue
		elif cont == 'y':
			bet = int(raw_input('How much will you bet this game? '))
			player.place_bet(bet)
			game = BlackJack(Deck(), player)
			game.play()
			print 'Current total: {0}'.format(player.bet_total)
		else:
			print 'Total Won: {0}'.format(player.bet_total)
			break
		