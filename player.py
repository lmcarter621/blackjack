class Player(object):
	"""Object representation of a player and their actions"""
	value_conversions = {'jack': 10, 'queen': 10, 'king': 10, 'ace': 11}
	def __init__(self):
		super(Player, self).__init__()
		self.hand = []
		self.hand_value = 0
		self.bets = []
		self.bet_total = 0

	def compute_hand_value(self):
		hand_value = 0
		for x in self.hand:
			# x -> (k, v)
			if isinstance(x[1], int):
				hand_value += x[1]
			else:
				hand_value += Player.value_conversions[x[1]]
				
		self.hand_value = hand_value

	def place_bet(self, bet):
		self.bets.append(bet)
		self.bet_total = 0;
		for bet in self.bets:
			self.bet_total += bet

	def hit(self, card):
		self.hand.append(card)
		self.compute_hand_value()

	# def double_down(self):
	# 	pass

	# def split(self):
	# 	pass

	# def surrender(self):
	# 	pass
	# 	