from aenum import Enum

class Suit(Enum):
	clubs = 1
	diamonds = 2
	hearts = 3
	spades = 4