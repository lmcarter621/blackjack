from random import randrange
from suit import Suit

class Deck(object):
	"""Array of """
	def __init__(self):
		super(Deck, self).__init__()
		#iterate through all the suits and iterate through all the numbers
		self.deck = {}
		for s in Suit:
			# print dir(s)
			self.deck[s._name_] = []
			for x in xrange(2,11):
				self.deck[s._name_].append(x)
			self.deck[s._name_].append('jack')
			self.deck[s._name_].append('queen')
			self.deck[s._name_].append('king')
			self.deck[s._name_].append('ace')

	def shuffle(self):
		deck_as_list = []
		shuffled_deck = []
		# create the list of items
		for k, v in self.deck.iteritems():
			for x in v:
				deck_as_list.append((k, x))

		# shuffle the list of items
		while deck_as_list != []:
			position = randrange(len(deck_as_list))
			shuffled_deck.append(deck_as_list[position])
			deck_as_list.pop(position)

		self.deck = shuffled_deck
		