class Card(object):
	"""A Card is defined by a Suit and Numerical value"""
	def __init__(self, suit, value):
		super(Card, self).__init__()
		self.suit = suit
		self.value = value

	def __str__(self):
		return '{0} of {1}'.format(self.value, self.suit)
		